# Load the deep learning and econometric predictions, assess the goodness of fit and 
# map the results.
# R 3.3 above

# Load the required libraries
x = c("plyr", "maptools", "RColorBrewer", "ggplot2", "gridExtra", "grid", "sp",
      "ggthemes", "rgdal", "cowplot")
lapply(x, library, character.only = TRUE)
verx = NULL
#x = NULL

# Set working directory (folder with the csv projection data)
# PLEASE CHANGE THIS TO YOUR WORKING DIRECTORY
setwd("/data/ye014/forest/map")

# Load the observed data and label it
realData = read.csv("realData.csv", header = FALSE)
names(realData) = c("id", "coords.X1", "coords.X2", "f2011", "f2012", "f2013", "f2014")

# load the deep learning predictions 
predictData = read.csv("predictData.csv", header = FALSE)
names(predictData) = c("id", "coords.X1", "coords.X2", "dl2011", "dl2012", "dl2013", "dl2014")
# Check that the data is in the [0,1] interval
summary(predictData)
# Apply bounds to the projections
predictData[, 4:7][predictData[, 4:7] < 0] = 0
predictData[, 4:7][predictData[, 4:7] > 1] = 1

# load the bootstrapped spatial panel with random effects predictions (Best Linear Unbiased Predictors)
predictEcon = read.csv("BLUP.csv")
predictEcon["X"] = NULL # remove useless column
names(predictEcon) = c("id", "ec2011", "ec2012", "ec2013", "ec2014")
# Check that the data is in the [0,1] interval
summary(predictEcon)

# R-squared (pseudo because the definition applies to linear models)
# Deep learning
(cor(predictData["dl2011"], realData["f2011"]))^2
(cor(predictData["dl2012"], realData["f2012"]))^2
(cor(predictData["dl2013"], realData["f2013"]))^2
(cor(predictData["dl2014"], realData["f2014"]))^2

# Bootstrapped spatial panel with random effects
(cor(predictEcon["ec2011"], realData["f2011"]))^2
(cor(predictEcon["ec2012"], realData["f2012"]))^2
(cor(predictEcon["ec2013"], realData["f2013"]))^2
(cor(predictEcon["ec2014"], realData["f2014"]))^2

# Merge the observed data and the projections into one dataframe
findex = realData
findex = merge(findex, predictData, by.x = "id", by.y = "id")
findex = merge(findex, predictEcon, by.x = "id", by.y = "id")
findex["coords.X1.y"] <- findex["coords.X2.y"]<- NULL
names(findex) = c("id", "coords.x1", "coords.x2", "f2011", "f2012", "f2013","f2014",
                  "dl2011", "dl2012", "dl2013", "dl2014", "ec2011",
                  "ec2012", "ec2013", "ec2014" )

# Prediction errors
findex["ec2011_error"] = findex[ "ec2011"] - findex[ "f2011"]
findex["ec2012_error"] = findex[ "ec2012"] - findex[ "f2012"]
findex["ec2013_error"] = findex[ "ec2013"] - findex[ "f2013"]
findex["ec2014_error"] = findex[ "ec2014"] - findex[ "f2014"]
findex["dl2011_error"] = findex[ "dl2011"] - findex[ "f2011"]
findex["dl2012_error"] = findex[ "dl2012"] - findex[ "f2012"]
findex["dl2013_error"] = findex[ "dl2013"] - findex[ "f2013"]
findex["dl2014_error"] = findex[ "dl2014"] - findex[ "f2014"]
# Averages
findex["avgEC_error"] = rowMeans(findex[,16:19])
findex["avgDL_error"] = rowMeans(findex[,20:23])
findex["avgObs"] = rowMeans(findex[,4:7])
findex["avgEC"] = rowMeans(findex[,12:15])
findex["avgDL"] = rowMeans(findex[,8:11])
# write.csv(findex, "findex_projections.csv")

# Remove original data sources to free some memory
realData <- predictData <- predictEcon <- NULL


# MAPS
# THIS SECTION IS MEMORY INTENSIVE. 
# You should run it on a cluster or at https://rstudio-01-cdc.it.csiro.au

# Import the AU boundary 
AU = readShapeSpatial("dotmap.shp")
#library(maptools)
#gpclibPermit()
au_DF <- fortify(AU, region = "dis") # convert the shapefile to a format that ggplot2 can read

#write.csv(au_DF, file='/home/ye014/R/au_DF.csv')



# Functions modified to run without cowplot and theme_map
 
# Function to extract the legend
get_legend<-function(myggplot){
  tmp <- ggplot_gtable(ggplot_build(myggplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}
 
# Import the AU boundary
# create a baseline layer
m0 <- ggplot(findex, aes(x=coords.x1, y=coords.x2))
 
# maps of average 2011-2014 projection ERRORS
DL_avg_E <- m0 +  geom_point(aes(colour = avgDL_error  ), shape = 15, size  = 0.001) +
  scale_colour_gradient2(name = "error",  limits=c(-1, 1)) +
  theme_void() +
  ggtitle("average error DL") + theme(legend.position = "right") +
  geom_polygon(data = au_DF, aes(long, lat, group=group), size = 0.2, colour="#bdbdbd", fill = NA)
# Save the legend
leg = get_legend(DL_avg_E)
# remove the legend from the first plot
DL_avg_E = DL_avg_E + theme(legend.position="none")
 
EC_avg_E <- m0 +   geom_point(aes(colour = avgEC_error), shape = 15, size  = 0.001) +
  scale_colour_gradient2(name = "error",  limits=c(-1, 1)) +
  theme_void() + 
  ggtitle("average error EC")  + 
  geom_polygon(data = au_DF, aes(long, lat, group=group), size = 0.2, colour="#bdbdbd", fill = NA)
# Save the legend
#leg = get_legend(EC_avg_E)
# remove the legend from the first plot
EC_avg_E = EC_avg_E + theme(legend.position="none")
 
# maps of average 2011-2014 findex, and DL and EC projections
OBS_avg <- m0 +  geom_point(aes(colour = avgObs), shape = 15, size  = 0.001) +
  scale_colour_gradient(low = "#e5f5f9", high = "#2ca25f",  name = "forest index") +
  theme_void() +
  ggtitle("average observed findex") + theme(legend.position = "right") +
  geom_polygon(data = au_DF, aes(long, lat, group=group), size = 0.2, colour="#bdbdbd", fill = NA)
# Save the legend
leg = get_legend(OBS_avg)
# remove the legend from the first plot
OBS_avg = OBS_avg + theme(legend.position="none")

DL_avg <- m0 +  geom_point(aes(colour = avgDL  ), shape = 15, size  = 0.001) +
  scale_colour_gradient(low = "#e5f5f9", high = "#2ca25f",  name = "forest index") +
  theme_void() +
  ggtitle("average projection DL")  + theme(legend.position = "right") +
  geom_polygon(data = au_DF, aes(long, lat, group=group), size = 0.2, colour="#bdbdbd", fill = NA)
# Save the legend
leg = get_legend(DL_avg)
# remove the legend from the first plot
DL_avg = DL_avg + theme(legend.position="none")

EC_avg <- m0 +   geom_point(aes(colour = avgEC), shape = 15, size  = 0.001) +
  scale_colour_gradient(low = "#e5f5f9", high = "#2ca25f",  name = "forest index") +
  #theme(legend.position = "right") + 
  theme_void() +
  ggtitle("average projection EC") + theme(legend.position = "right") +
  geom_polygon(data = au_DF, aes(long, lat, group=group), size = 0.2, colour="#bdbdbd", fill = NA)
# Save the legend
#leg =  get_legend(EC_avg)
# remove the legend from the first plot
EC_avg = EC_avg + theme(legend.position='none')

#OBS_avg
#DL_avg
#EC_avg
 
# grid
# with this command you can plot a grid of two or more plots.
# To add more than two plots just write their names and modyfy the columns and rows numbers
maps = grid.arrange(DL_avg_E, EC_avg_E, leg,  ncol=3, nrow =1, widths = c(2.3, 2.3, 0.8))
 
ggsave("maps EC vs DL.tiff", plot = maps, dpi = 500, width = 190, height = 75, units = "mm",
       compression="lzw", type="cairo")
 
# Example to create 2x2 grid
mapsTest = grid.arrange(DL_avg, EC_avg, OBS_avg, DL_avg_E, EC_avg_E, leg, ncol=3, nrow =2)
 
ggsave("maps EC vs DL TEST01.tiff", plot = mapsTest, dpi = 500, width = 190, height = 150, units = "mm",
       compression="lzw", type="cairo")
