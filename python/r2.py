import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.stats import norm
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
import scipy.stats.stats as stats
from scipy.stats.stats import pearsonr
from sklearn.metrics import r2_score
import sklearn.metrics as metrics

import statsmodels.api as sm


import csv
import pandas as pd

factData_file = 'data/factData/fact.csv'
predictData_file = 'data/predictData/predict.csv'
result_file = './r2.csv'
#predictData_filename = './staticOutput.csv'

#----------Load Data------------------------------
fact_reader = pd.read_csv(filepath_or_buffer=factData_file, delimiter=',')
factData_temp = np.array(fact_reader)
factData = factData_temp.transpose()

predict_reader = pd.read_csv(filepath_or_buffer=predictData_file, delimiter=',')
predictData_temp = np.array(predict_reader)
predictData = predictData_temp.transpose()

#-------------Mean Square Error-------------------
def rmse(y_test, y):
    return sp.sqrt(sp.mean((y_test - y) ** 2))

#---------------R Square--------------------------
#-----[0, 1]--------------------------------------
#--------------Version 01-------------------------
def R2(y_test, y_true):
    return 1-((y_test - y_true) ** 2).sum() / ((y_true - y_true.mean()) ** 2).sum()
#--------------Version 02-------------------------
def R22(y_test, y_true):
    y_mean = np.array(y_true)
    y_mean[:] = y_mean.mean()
    return 1 - rmse(y_test, y_true) / rmse(y_mean, y_true)
#--------------Version 03-------------------------
def R23(y_test, y_true):
    sst_temp = np.zeros((len(y_true), len(factData_temp)))
    sst = np.zeros(len(y_true))
    sse_temp = np.zeros((len(y_test), len(predictData_temp)))
    sse = np.zeros(len(y_test))
    for j in range(len(y_test)):
        _sse_temp = y_test[j] - y_true[j]
        sse_temp[j] = sse_temp[j] + np.square(_sse_temp)
        sse[j] = sse[j] + np.sum(sse_temp[j])
    for k in range(len(y_true)):
        _sst_temp = y_true[k]
        sst_temp[k] = sst_temp[k] + np.square(_sst_temp)
        sst[k] = sst[k] + np.sum(_sst_temp)
    # return 1 - sp.sqrt(sse / sst)
    return 1 - sse / sst

#--------------Squred Correlation Coefficient-------------------------
#-----------Pearson correlation coefficient--------------
def pearson(y_test,y_true):
    return pearsonr(y_test, y_true)
    # return np.corrcoef(y_test,y_true)[0,1]

# plt.scatter(x, y, s=5)
# degree = [1,2,100]
# y_test = []
# y_test = np.array(y_test)
#
# for d in degree:
#     clf = Pipeline([('ploy', PolynomialFeatures(degree=d)),
#                      ('linear', LinearRegression(fit_intercept=False))])
#     clf.fit(x[:, np.newaxis], y)
#     y_test = clf.predict(x[:, np.newaxis])
#
#     print(clf.named_steps['linear'].coef_)
#     print('rmse=%.2f, R2=%.2f, R22=%.2f, clf.score=%.2f' %
#           (rmse(y_test, y),
#            R2(y_test, y),
#            R22(y_test, y),
#            clf.score(x[:, np.newaxis], y)))
resultData = []
for i in range(len(predictData)):
    yearVal = 'Year of 201'+ str(i+1)
    rmseVal = rmse(predictData[i], factData[i])
    r2Val = R2(predictData[i], factData[i])
    r22Val = R22(predictData[i], factData[i])
    r23Val = R23(predictData, factData)[i]
    print('Year of 201'+ str(i+1))
    print('rmse=%.4f, R2=%.4f, R22=%.4f, R23=%.4f' %
          (rmseVal, r2Val, r22Val, r23Val))
    print(stats.linregress(predictData[i], factData[i]))
    result = (rmse(predictData[i], factData[i]), R2(predictData[i], factData[i]), R22(predictData[i], factData[i]), R23(predictData, factData)[i])
    resultData.append(result)
    np.savetxt(result_file, resultData, fmt='%.4f', delimiter=',')


