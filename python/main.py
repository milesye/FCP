# Python2.7 + Tensorflow 1.1.0
# to process the sparial features
import numpy as np
import pandas as pd
from pandas import Series
# from neon import NervanaObject
# from math import sqrt
# from sklearn.metrics import mean_squared_error
# from sklearn.preprocessing import StandardScaler
# from IPython.display import Image

# from bokeh.io import vplot, gridplot
# import bokeh.plotting as bk

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python import SKCompat

import sys


if __name__ == '__main__':
    init = tf.global_variables_initializer()

    # ED, temp5MA, rain, price_rec, timber_price_index, ftr2 = load_data()
    tenure, aria, slope, elevation, dist2prote, profit06, pawc, ph30, clay30, bulkd30, fidx10NN, fidx3NN, ED, rain5SD, rain5MA, temp5MA, temp5SD, rain, tmax, price_rec, timber_price_index, ftr2 = load_data()

    predictData = []
    # allVal =[]

    task_ID = int(sys.argv[1])
    start = (task_ID-1)*RECORD_NUM
    end = (task_ID)*RECORD_NUM

    predictData_filename = 'data/predictData'+ str(task_ID) + '.csv'
    factData_filename = 'data/factData'+ str(task_ID) + '.csv'

    with tf.Session() as sess:
        sess.run(init)
        for i in range(RECORD_NUM):
            # xy = get_data(ED, temp5MA, rain, price_rec, timber_price_index, ftr2, i)
            xy = get_data(tenure, aria, slope, elevation, dist2prote, profit06, pawc, ph30, clay30, bulkd30, fidx10NN, fidx3NN, ED, rain5SD, rain5MA, temp5MA, temp5SD, rain, tmax, price_rec, timber_price_index, ftr2, i)
            X = xy[:, :-1]
            y = xy[:, [-1]]

            #trainX(15, data_dim-1), trainY(15, 1)
            trainX, trainY, testX, testY = split_Xy(X, y, test_rows)

            # ----------use learn.Estimator and build the custom model-----------------------
            # watch out: in tensorflow steps are not epochs!
            # Steps are the number of minibatches to process
            n_input = trainX.shape[1]
            # steps is the number of batches to process
            steps = (trainX.shape[0] / batch_size) * epochs

            model = SKCompat(learn.Estimator(model_fn=lstm_model, model_dir=LOG_DIR, params={'learning_rate': Learning_rate}))
            # model = SKCompat(
            #     learn.Estimator(model_fn=lstm_model, params={'learning_rate': Learning_rate}))

            #---Not all TensorFlow operations support float64, convert all variables to type float32-------------
            trainX = trainX.astype(np.float32).copy()
            trainY = trainY.astype(np.float32).copy()
            testX = testX.astype(np.float32).copy()
            testY = testY.astype(np.float32).copy()
            X, y = X.astype(np.float32).copy(), y.astype(np.float32).copy()

            # ------------fit the model---------------------------------------------------------------
            # make sure to create the tmp directory if it doesn't exist.
            model.fit(trainX, trainY, steps=steps)

            y_test_predicted = model.predict(testX)
            testPredict = y_test_predicted.reshape(4, )
            #If x>1.0 --> x=1.0; If x<0.0 --> x=0.0
            testPredict = map(lambda x: np.where(x > 1.0, 1.0, np.where(x >= 0.0, x, 0.0)), testPredict)
            predictData.append(testPredict)
            print(i)
            np.savetxt('data/predictData.csv', predictData, fmt='%.5f', delimiter=',')