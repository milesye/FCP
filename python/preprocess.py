import numpy as np
import pandas as pd
from pandas import Series
# from neon import NervanaObject
# from math import sqrt
# from sklearn.metrics import mean_squared_error
# from sklearn.preprocessing import StandardScaler
# from IPython.display import Image

# from bokeh.io import vplot, gridplot
# import bokeh.plotting as bk

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python import SKCompat

# from lstm import lstm_model

# import matplotlib.pyplot as plt

import sys

# Forest data
FILEPATH = '/data/Forest/forestDataSample.csv'
# Economic data
PATH2 = '/data/Forest/economic_data.csv'

# data normalization scaling to (-1, 1)
# label_col: need not consider column label, string or list
# type: float64
def data_normalization(df, label_col = []):
    lab_len = len(label_col)
    print label_col
    if lab_len>0:
        df_temp = df.drop(label_col, axis = 1)
        df_lab = df[label_col]
        print df_lab
    else:
        df_temp = df
    max_val = list(df_temp.max(axis=0))
    min_val = list(df_temp.min(axis=0))
    mean_val = list((df_temp.max(axis=0) + df_temp.min(axis=0)) / 2)
    nan_values = df_temp.isnull().values
    row_num = len(list(df_temp.values))
    col_num = len(list(df_temp.values)[1])
    for rn in range(row_num):
        #data_values_r = list(data_values[rn])
        nan_values_r = list(nan_values[rn])
        for cn in range(col_num):
            if nan_values_r[cn] == False:
                df_temp.values[rn][cn] = 2 * (df_temp.values[rn][cn] - mean_val[cn])/(max_val[cn] - min_val[cn])
            else:
                print 'Wrong'
    for index,lab in enumerate(label_col):
        df_temp.insert(index, lab, df_lab[lab])
    return df_temp

def load_data():
    #------------------Load Forest data(10+11)----------------------------------------------------
    data_raw = pd.read_csv(FILEPATH)
    data_raw = data_raw.astype(float)

    #-----------------Spacial Properties Data-----------------------------------------------
    dates = Series(['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007',
                  '2008', '2009', '2010', '2011', '2012', '2013', '2014'])
    # 1.Tenure Type
    df = pd.DataFrame(data_raw).ix[:,2:3]
    df = df.T
    tenure = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    tenure.iloc[0:19,:] = df.values
    # 2.Accessibility and Remoteness Index
    df = pd.DataFrame(data_raw).ix[:,4:5]
    df = df.T
    aria = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    aria.iloc[0:19,:] = df.values
    # 3.Slope
    df = pd.DataFrame(data_raw).ix[:,5:6]
    df = df.T
    slope = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    slope.iloc[0:19,:] = df.values
    # 4.elevation
    df = pd.DataFrame(data_raw).ix[:,6:7]
    df = df.T
    elevation = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    elevation.iloc[0:19,:] = df.values
    # 5.distance to protected areas
    df = pd.DataFrame(data_raw).ix[:,7:8]
    df = df.T
    dist2prote = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    dist2prote.iloc[0:19,:] = df.values
    # 6.potential agricultural profit
    df = pd.DataFrame(data_raw).ix[:,8:9]
    df = df.T
    profit06 = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    profit06.iloc[0:19,:] = df.values
    # 7.Plant available water capacity
    df = pd.DataFrame(data_raw).ix[:,10:11]
    df = df.T
    pawc = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    pawc.iloc[0:19,:] = df.values
    # 8.PH
    df = pd.DataFrame(data_raw).ix[:,11:12]
    df = df.T
    ph30 = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    ph30.iloc[0:19,:] = df.values
    # 9.clay content
    df = pd.DataFrame(data_raw).ix[:,12:13]
    df = df.T
    clay30 = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    clay30.iloc[0:19,:] = df.values
    # 10.bulk density
    df = pd.DataFrame(data_raw).ix[:,2:3]
    df = df.T
    bulkd30 = pd.DataFrame(np.random.randn(19, totalNUM), index=dates)
    bulkd30.iloc[0:19,:] = df.values

    #------------Time Series data----------------------------------------------------
    # 1.Average forest index 10Km
    df = pd.DataFrame(data_raw).ix[:,19:38]
    df = data_normalization(df)
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    fidx10NN = df.T

    # 2.Average forest index 3Km
    df = pd.DataFrame(data_raw).ix[:,38:57]
    df = data_normalization(df)
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    fidx3NN = df.T

    # 3.Euclidean distance
    df = pd.DataFrame(data_raw).ix[:,67:94]
    df = data_normalization(df)
    df.__delitem__('ED1990')
    df.__delitem__('ED1993')
    df.__delitem__('ED1994')
    df.__delitem__('ED1996')
    df.__delitem__('ED1997')
    df.__delitem__('ED1999')
    df.__delitem__('ED2001')
    df.__delitem__('ED2003')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    ED = df.T
    # read_col = ED.loc[:, 1]

    # 4.Rainfall, five-year moving standard deviation
    df = pd.DataFrame(data_raw).ix[:,94:128]
    df = data_normalization(df)
    df.__delitem__('rain5SD83')
    df.__delitem__('rain5SD84')
    df.__delitem__('rain5SD85')
    df.__delitem__('rain5SD86')
    df.__delitem__('rain5SD87')
    df.__delitem__('rain5SD90')
    df.__delitem__('rain5SD93')
    df.__delitem__('rain5SD94')
    df.__delitem__('rain5SD96')
    df.__delitem__('rain5SD97')
    df.__delitem__('rain5SD99')
    df.__delitem__('rain5SD01')
    df.__delitem__('rain5SD03')
    df.__delitem__('rain5SD15')
    df.__delitem__('rain5SD16')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    rain5SD = df.T

    # 5.Rainfall, five-year moving averages
    df = pd.DataFrame(data_raw).ix[:,128:161]
    df = data_normalization(df)
    df.__delitem__('rain5MA83')
    df.__delitem__('rain5MA84')
    df.__delitem__('rain5MA85')
    df.__delitem__('rain5MA86')
    df.__delitem__('rain5MA87')
    df.__delitem__('rain5MA90')
    df.__delitem__('rain5MA93')
    df.__delitem__('rain5MA94')
    df.__delitem__('rain5MA96')
    df.__delitem__('rain5MA97')
    df.__delitem__('rain5MA99')
    df.__delitem__('rain5MA01')
    df.__delitem__('rain5MA03')
    df.__delitem__('rain5MA15')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    rain5MA = df.T

    # 6.Temperature, five-year moving averages
    df = pd.DataFrame(data_raw).ix[:,161:188]
    df = data_normalization(df)
    df.__delitem__('temp5MA90')
    df.__delitem__('temp5MA93')
    df.__delitem__('temp5MA94')
    df.__delitem__('temp5MA96')
    df.__delitem__('temp5MA97')
    df.__delitem__('temp5MA99')
    df.__delitem__('temp5MA01')
    df.__delitem__('temp5MA03')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    temp5MA = df.T

    # 7.Temperature, five-year moving standard deviation
    df = pd.DataFrame(data_raw).ix[:,188:215]
    df = data_normalization(df)
    df.__delitem__('temp5SD90')
    df.__delitem__('temp5SD93')
    df.__delitem__('temp5SD94')
    df.__delitem__('temp5SD96')
    df.__delitem__('temp5SD97')
    df.__delitem__('temp5SD99')
    df.__delitem__('temp5SD01')
    df.__delitem__('temp5SD03')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    temp5SD = df.T

    # 8.Annual total Rainfall
    df = pd.DataFrame(data_raw).ix[:,215:248]
    df = data_normalization(df)
    df.__delitem__('rain1983')
    df.__delitem__('rain1984')
    df.__delitem__('rain1985')
    df.__delitem__('rain1986')
    df.__delitem__('rain1987')
    df.__delitem__('rain1990')
    df.__delitem__('rain1993')
    df.__delitem__('rain1994')
    df.__delitem__('rain1996')
    df.__delitem__('rain1997')
    df.__delitem__('rain1999')
    df.__delitem__('rain2001')
    df.__delitem__('rain2003')
    df.__delitem__('rain2015')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    rain = df.T

    # 9.Average maximum temperature
    df = pd.DataFrame(data_raw).ix[:,248:281]
    df = data_normalization(df)
    df.__delitem__('tmax_1983')
    df.__delitem__('tmax_1984')
    df.__delitem__('tmax_1985')
    df.__delitem__('tmax_1986')
    df.__delitem__('tmax_1987')
    df.__delitem__('tmax_1990')
    df.__delitem__('tmax_1993')
    df.__delitem__('tmax_1994')
    df.__delitem__('tmax_1996')
    df.__delitem__('tmax_1997')
    df.__delitem__('tmax_1999')
    df.__delitem__('tmax_2001')
    df.__delitem__('tmax_2003')
    df.__delitem__('tmax_2015')
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    tmax = df.T

    #Average forest index 10Km
    df = pd.DataFrame(data_raw).ix[:,283:302]
    # df = data_normalization(df)
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    ftr2 = df.T

    #------------------Load Economic data--------------
    economic_data = pd.read_csv(PATH2)
    economic_data = data_normalization(economic_data)
    data_raw = economic_data.astype(float)
    data_raw = data_raw.T

    # 10.Index of Price Received by Farmers(2013)
    df = pd.DataFrame(data_raw).ix[0:1]
    # df = ec_data_normal(df)
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    price_rec = df.T
    # 11.Timber Price Index
    df = pd.DataFrame(data_raw).ix[1:2]
    # df = ec_data_normal(df)
    df.columns = ['1988', '1989', '1991', '1992', '1995', '1998', '2000', '2002', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014']
    timber_price_index = df.T

    # tenure, aria, slope, elevation, dist2prote, profit06, pawc, ph30, clay30, bulkd30
    # fidx10NN, fidx3NN, ED, rain5SD, rain5MA, temp5MA, temp5SD, rain, tmax
    # price_rec, timber_price_index
    # ftr2
    return tenure, aria, slope, elevation, dist2prote, profit06, pawc, ph30, clay30, bulkd30, fidx10NN, fidx3NN, ED, rain5SD, rain5MA, temp5MA, temp5SD, rain, tmax, price_rec, timber_price_index, ftr2

# def get_data(ED, temp5MA, rain, price_rec, timber_price_index, ftr2, read_col):
def get_data(tenure, aria, slope, elevation, dist2prote, profit06, pawc, ph30, clay30, bulkd30, fidx10NN, fidx3NN, ED, rain5SD, rain5MA, temp5MA, temp5SD, rain, tmax, price_rec, timber_price_index, ftr2, read_col):
    # ------------Spacial data----------------------------------------------------------
    col_S01 = tenure.loc[:, read_col]
    col_S02 = aria.loc[:, read_col]
    col_S03 = slope.loc[:, read_col]
    col_S04 = elevation.loc[:, read_col]
    col_S05 = dist2prote.loc[:, read_col]
    col_S06 = profit06.loc[:, read_col]
    col_S07 = pawc.loc[:, read_col]
    col_S08 = ph30.loc[:, read_col]
    col_S09 = clay30.loc[:, read_col]
    col_S10 = bulkd30.loc[:, read_col]
    # ------------Time Series data--------------------------------------------------------
    col01 = fidx10NN.loc[:, read_col]
    col02 = fidx3NN.loc[:, read_col]
    col03 = ED.loc[:, read_col]
    col04 = rain5SD.loc[:, read_col]
    col05 = rain5MA.loc[:, read_col]
    col06 = temp5MA.loc[:, read_col]
    col07 = temp5SD.loc[:, read_col]
    col08 = rain.loc[:, read_col]
    col09 = tmax.loc[:, read_col]
    col10 = price_rec.iloc[:, 0]
    col11 = timber_price_index.iloc[:, 0]
    # ------------Target data-------------------------------------------------------------
    col12 = ftr2.loc[:, read_col]
    # ------------Organize pandas dataframe------------------------------------------------
    test = pd.DataFrame(
        [col_S01, col_S02, col_S03, col_S04, col_S05, col_S06, col_S07, col_S08, col_S09, col_S10, col01, col02, col03,
         col04, col05, col06, col07, col08, col09, col10, col11, col12])
    # test = pd.DataFrame([col02, col03, col04, col05, col06, col07, col08, col09, col10, col11, col12])
    test = test.T
    test = test.as_matrix(columns=None)
    return test

#split the dataset in training and test
def split_Xy(X, y, setTestRows):
    #Train Rows: 1988-2010
    X_train = X[:-setTestRows]
    y_train = y[:-setTestRows]
    #Test Rows: 2011-2014
    X_test = X[-setTestRows:]
    y_test = y[-setTestRows:]

    return X_train, y_train, X_test, y_test