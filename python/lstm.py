# Python2.7 + Tensorflow 1.1.0
# to process the sparial features
import numpy as np
import pandas as pd
from pandas import Series
# from neon import NervanaObject
# from math import sqrt
# from sklearn.metrics import mean_squared_error
# from sklearn.preprocessing import StandardScaler
# from IPython.display import Image

# from bokeh.io import vplot, gridplot
# import bokeh.plotting as bk

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python import SKCompat

import sys

# ckptDir = 'checkpoint/'
# modelCkptGraphFile = 'checkpoint/Solution02/model.ckpt-1500.meta'
# modelCkptFile = 'checkpoint/Solution02/model.ckpt-1500'
# checkpointDir = 'checkpoint/Solution02/'

#number of record to be processed in spatial data
totalNUM = 9
#number of record to be processed in LSTM
RECORD_NUM = 9

# seq_length = 1
test_rows = 4                #Set test row number
#test_size = 0.2             #test perchentage

#the number of data [read from trainning data] entered into ONE epochs
batch_size = 15
#the number of full iterations of trainning data, how many time the algorithm is going to run
#affects directly (or not) the result of the training step
epochs = 100

LOG_DIR = './checkpoint/Solution02'
Learning_rate = 0.01
#Time Steps
#the number of sequence to process
# sequence length,represents the number of elements that we would like to use when classifying a sequence
n_steps = 1
#dim hidden, neurons number of hidden layer in each of the LSTM cells
hidden = 128
#Dropout probability
keep_prob = 0.1

#Numbers of hidden layer is (data_dim-1)
#input features number + output predictor
data_dim = 11
#output predictor
# output_dim = 1

#prediction steps, 1 hour in the future given a sampling rate of 5 minutes
# prediction = 12
# steps_forward = prediction
#must be negtive or zero
# steps_backward = 0      
# inputs_default = 0
# hidden = 128
# n_steps = 1
# RNN_LAYERS = [{'num_units': 139}, {'num_units': 69}]
# DENSE_LAYERS = None     #[10, 10]

def variable_summaries(var):
  """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)

#build an LSTM Neural Network with a single layer and hidden number of neurons
def lstm_model(features, targets, mode, params):
    #-----------Model function for Estimator-----------------------------------
    #n_steps: the number of sequence to process, = 1
    #n_input: input_dim, the number of input features  i.e. # Steps are the number of minibatches to process
    features = tf.reshape(features, [-1, n_steps, n_input])  # (batch_size, n_steps, n_input)

    with tf.variable_scope('LSTM'):
        #num_units hidden=128

        lstm_cell = tf.contrib.rnn.LSTMBlockCell(hidden)
        lstm_cell = tf.contrib.rnn.DropoutWrapper(lstm_cell, output_keep_prob=keep_prob)
        #the numbers of hidden layer is (data_dim-1)
        cell = tf.contrib.rnn.MultiRNNCell([lstm_cell] * (data_dim-1))
        (output, state) = tf.nn.dynamic_rnn(cell = cell, inputs=features, dtype=tf.float32)

        print('lstm cell output %s' % output)
        # temp = output.eval()
        # print('lstm cell output %.3f' % temp)

    #targets or y are [None, n_classes], n_classes is types of results
    print('targets shape %s' % targets)
    target_shape = targets.get_shape()
    if len(target_shape) == 1:
        output_shape = 1
        targets = tf.reshape(targets, [-1, 1])
    else:
        output_shape = target_shape[1]

    # output shape: (time_step_size, batch_size, input_vec_size)
    output = tf.transpose(output, [1, 0, 2])                #permute n_steps and batch_size
    last = tf.gather(output, int(output.get_shape()[0]) - 1)
    print('last shape %s' % last)

    init = tf.random_normal_initializer(0, 0.1)

    # with tf.name_scope('weights'):
    weights = tf.get_variable('weights', [last.get_shape()[1], output_shape],
                              initializer=init,
                              dtype=tf.float32)
    # variable_summaries(weights)
    # with tf.name_scope('bias'):
    bias = tf.get_variable('bias', [output_shape],
                           initializer=init,
                           dtype=tf.float32)
    # variable_summaries(bias)


    #output(?*128) x weights(128,1) + bias(1*?)
    predictions = tf.nn.xw_plus_b(last, weights, bias)

    # Calculate loss using mean squared error

    with tf.name_scope('loss'):
        # loss = tf.contrib.losses.mean_squared_error(predictions, targets)
        loss = tf.losses.mean_squared_error(predictions, targets)

    with tf.name_scope('train'):
        train_op = tf.contrib.layers.optimize_loss(
            loss=loss,
            global_step=tf.contrib.framework.get_global_step(),
            learning_rate=params["learning_rate"],
            optimizer="SGD")

    return predictions, loss, train_op